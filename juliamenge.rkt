#lang racket
(require 2htdp/image)
(define-struct complexNumber (real imag))

(define (getPixel maxIter currentHeigth currentWidth maxHeigth maxWidth zoom rMax)
  (generateColor (counterFct (make-complexNumber (exact->inexact (/ (- (- currentWidth (* zoom 0.306005)) (/ maxWidth 2)) zoom))
                                                 (exact->inexact (/ (- (- currentHeigth (* zoom 0.1)) (/ maxHeigth 2)) zoom)))
                             (make-complexNumber -0.9 0.28)
                             maxIter
                             rMax)
                 maxIter)
)

(define (counterFct complexZ complexC n rMax)
  (if (or (> (absComp (complexNumber-real complexZ) (complexNumber-imag complexZ)) rMax)
          (= n 0))
      0
      (+ 1 (counterFct (addComp (multComp (complexNumber-real complexZ)
                                          (complexNumber-imag complexZ))
                                complexC)
                       complexC
                       (sub1 n)
                       rMax)
         )
      )
)

(define (multComp real imag)
  (make-complexNumber (- (* real real)
                         (* imag imag))
                      (* (* imag real) 2)
  )
)

(define (addComp complex1 complex2)
  (make-complexNumber (+ (complexNumber-real complex1)
                         (complexNumber-real complex2))
                      (+ (complexNumber-imag complex1)
                         (complexNumber-imag complex2)))
)

(define (absComp real imag)
  (sqrt (+ (* real real)
           (* imag imag)))
)

(define (forEachPixel maxIter maxHeigth maxWidth zoom rMax)
  (apply append
    (for/list ([h (build-list maxHeigth values)])
      (for/list ([w (build-list maxWidth values)])
        (begin
          ;(printf (string-append (number->string h) "-" (number->string w)))
          ;(newline)
          (getPixel maxIter h w maxHeigth maxWidth zoom rMax)
        )
      )
    )
  )
)

(define (generateJulia heigth width zoom maxIter rMax name)
  (save-image (color-list->bitmap (forEachPixel maxIter heigth width zoom rMax) width heigth) name)
)

(define (generateColor counter maxIter)
  (local
    (
     (define H (* 60 (/ counter 570)))
     (define V (if (< counter maxIter) 1 0))
     (define hi (floor H))
     (define f (- H hi))
     (define rg (floor (if (= V 0 ) 0 (* 255 (/ H 60)(/ H 60)))))
     (define q (floor (* 255 (* V (- 1 f)))))
     (define t (floor (* 255 (* V f))))
     (define b (floor (if (= V 0 ) 0 (* 255 (/ H 60)))))
    )

    (make-color (if (> (* 6 rg) 255) 255 (* 6 rg)) (if (> (* rg 6) 255) 255 (* rg 6)) (if (> (* 6 b) 255) 255 (* 6 b)))
    
    ;(cond
    ;  [(= hi 1) (make-color q (* 255 V) 0)]
    ;  [(= hi 2) (make-color 0 (* 255 V) t)]
    ;  [(= hi 3) (make-color 0 q (* 255 V))]
    ;  [(= hi 4) (make-color t 0 (* 255 V))]
    ;  [(= hi 5) (make-color (* 255 V) 0 q)]
    ;  [else (make-color (* 255 V) t 0)]
    ;)
  )
)

(for ([x (build-list 1500 values)])
  (generateJulia 360 480 (+ 100 (expt 2 (+ 2 (/ x 4)))) 570 32 (string-append (number->string x) "JuliaMenge.png"))
)
