# Juliamenge

## About
Written in Racket as a small test project. Features a script for the julia set and one for the mandelbrot set.

### Example:
![screen-gif](./140JuliaMenge.gif)
